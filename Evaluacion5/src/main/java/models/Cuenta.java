package models;

import java.util.List;
	// Clase Cuenta
public class Cuenta {
    private int id;
    private String nombreUsuario;
    private double saldo;
    private String moneda;
    private String tipoCuenta;
    private List<Transaccion> transacciones; // Lista de transacciones asociadas a la cuenta
    // Constructor
    public Cuenta(int id, String nombreUsuario, double saldo, String moneda, String tipoCuenta, List<Transaccion> transacciones) {
        this.id = id;
        this.nombreUsuario = nombreUsuario;
        this.saldo = saldo;
        this.moneda = moneda;
        this.tipoCuenta = tipoCuenta;
        this.transacciones = transacciones;
    }

    // Getters y Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    public List<Transaccion> getTransacciones() {
        return transacciones;
    }

    public void setTransacciones(List<Transaccion> transacciones) {
        this.transacciones = transacciones;
    }
}
