package models;

public class Usuario {

    private int id;
    private String nombre;
    private String apellido;
    private double saldo;
    private String contrasena;
    
	//constructor
    public Usuario(int id, String nombre, String apellido, double saldo, String contrasena) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.saldo = saldo;
		this.contrasena = contrasena;
	}
	// Getters y Setters
	
	public int getId() {
		return id;
	}
	
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
		System.out.println("Prueba");

	}

    
}
