package models;

import java.util.Date;

public class Transaccion {
    private int id;
    private String tipo; // Por ejemplo: "Dep�sito", "Retiro", "Transferencia"
    private double monto;
    private Date fecha;
    
	public Transaccion(int id, String tipo, double monto, Date fecha) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.monto = monto;
		this.fecha = fecha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

    
}