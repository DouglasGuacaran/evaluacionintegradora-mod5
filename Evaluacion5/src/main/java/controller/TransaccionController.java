package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import conexion.ConexionBD;
import dao.CuentaDAO;
import models.Usuario;

@WebServlet("/transaccion")
public class TransaccionController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = ConexionBD.getConexion();
            CuentaDAO cuentaDAO = new CuentaDAO(conn);
            Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
            if (usuario == null) {
                response.sendRedirect("index.jsp");
                return;
            }
            int idUsuario = usuario.getId();
            String tipoOperacion = request.getParameter("tipoOperacion");
            double monto = Double.parseDouble(request.getParameter("monto"));
            double saldoActual = cuentaDAO.obtenerSaldo(idUsuario);

            if ("deposito".equals(tipoOperacion)) {
                cuentaDAO.actualizarSaldo(idUsuario, saldoActual + monto);
                request.setAttribute("mensaje", "Dep�sito exitoso. Saldo actual: " + (saldoActual + monto));
            } else if ("retiro".equals(tipoOperacion)) {
                if (saldoActual >= monto) {
                    cuentaDAO.actualizarSaldo(idUsuario, saldoActual - monto);
                    request.setAttribute("mensaje", "Retiro exitoso. Saldo actual: " + (saldoActual - monto));
                } else {
                    request.setAttribute("mensaje", "Fondos insuficientes.");
                }
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/vista/Home.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error al realizar la transacci�n.");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
