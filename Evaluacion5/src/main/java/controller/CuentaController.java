package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conexion.ConexionBD;
import dao.CuentaDAO;
import models.Cuenta;

@WebServlet("/verSaldo")
public class CuentaController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
    	
    	Connection conn = null;
    	
        try {
        	conn = ConexionBD.getConexion();
        	CuentaDAO cuentaDAO = new CuentaDAO(conn);
        	int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
			double saldo = cuentaDAO.obtenerSaldo(idUsuario);
			request.setAttribute("saldo", saldo);
						
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if (conn != null){
					conn.close();
				}
			} catch (Exception e2) {
				
			}
		}
    	    	
    }
}
