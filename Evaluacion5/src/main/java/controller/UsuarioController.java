package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import conexion.ConexionBD;
import dao.UsuarioDAO;
import models.Usuario;

@WebServlet("/UsuarioController")
public class UsuarioController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
        
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String action = request.getParameter("action");
        if ("login".equals(action)) {
            handleLogin(request, response);
        } else if ("register".equals(action)) {
            handleRegister(request, response);
        }
	}
    
    private void handleLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = ConexionBD.getConexion();
            UsuarioDAO usuarioDAO = new UsuarioDAO(conn);
            String nombre = request.getParameter("nombre");
            String contrasena = request.getParameter("contrasena");
            Usuario usuario = usuarioDAO.login(nombre, contrasena);
            if (usuario != null) {
                HttpSession session = request.getSession();
                session.setAttribute("usuario", usuario);
                response.sendRedirect("vista/Home.jsp");
            } else {
                request.setAttribute("mensaje", "Usuario o contrase�a incorrectos");
                RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error en el inicio de sesi�n.");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleRegister(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = ConexionBD.getConexion();
            UsuarioDAO usuarioDAO = new UsuarioDAO(conn);
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String contrasena = request.getParameter("contrasena");
            double saldo = Double.parseDouble(request.getParameter("saldo"));
            Usuario usuario = new Usuario(0, nombre, apellido, saldo, contrasena); // Ajusta seg�n tu constructor de Usuario
            usuarioDAO.agregarUsuario(usuario);
            request.setAttribute("mensaje", "Registro exitoso, ahora puede iniciar sesi�n");
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error en el registro.");
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}