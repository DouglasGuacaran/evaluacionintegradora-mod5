package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Cuenta;

public class CuentaDAO {
    private Connection conexion;

    public CuentaDAO(Connection conexion) {
        this.conexion = conexion;
    }

 // M�todo para obtener el saldo de la cuenta de un usuario espec�fico
    public double obtenerSaldo(int idUsuario) throws SQLException {
        double saldo = 0.0;
        String consulta = "SELECT saldo FROM usuario WHERE id = ?";
        try (PreparedStatement ps = conexion.prepareStatement(consulta)) {
            ps.setInt(1, idUsuario);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    saldo = rs.getDouble("saldo");
                }
            }
        }
        return saldo;
    }
 // M�todo para actualizar el saldo de la cuenta de un usuario espec�fico
    public void actualizarSaldo(int idUsuario, double nuevoSaldo) throws SQLException {
        String consulta = "UPDATE usuario SET saldo = ? WHERE id = ?";
        try (PreparedStatement ps = conexion.prepareStatement(consulta)) {
            ps.setDouble(1, nuevoSaldo);
            ps.setInt(2, idUsuario);
            ps.executeUpdate();
        }
    }
    
}
