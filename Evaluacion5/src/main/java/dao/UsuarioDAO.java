package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Usuario;

public class UsuarioDAO {
    private Connection conexion;

    public UsuarioDAO(Connection conexion) {
        this.conexion = conexion;
    }

    public Usuario login(String nombre, String contrasena) throws SQLException {
        String consulta = "SELECT * FROM usuario WHERE nombre = ? AND contrasena = ?";
        try (PreparedStatement ps = conexion.prepareStatement(consulta)) {
            ps.setString(1, nombre);
            ps.setString(2, contrasena);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return new Usuario(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("apellido"),
                        rs.getDouble("saldo"),
                        rs.getString("contrasena")
                    );
                }
            }
        }
        return null;
    }

    public void agregarUsuario(Usuario usuario) throws SQLException {
        String consulta = "INSERT INTO usuario (nombre, apellido, saldo, contrasena) VALUES (?, ?, ?, ?)";
        try (PreparedStatement ps = conexion.prepareStatement(consulta)) {
            ps.setString(1, usuario.getNombre());
            ps.setString(2, usuario.getApellido());
            ps.setDouble(3, usuario.getSaldo());
            ps.setString(4, usuario.getContrasena());
            ps.executeUpdate();
        }
    }

    public void actualizarUsuario(Usuario usuario) throws SQLException {
        String consulta = "UPDATE usuario SET nombre = ?, apellido = ?, saldo = ?, contrasena = ? WHERE id = ?";
        try (PreparedStatement ps = conexion.prepareStatement(consulta)) {
            ps.setString(1, usuario.getNombre());
            ps.setString(2, usuario.getApellido());
            ps.setDouble(3, usuario.getSaldo());
            ps.setString(4, usuario.getContrasena());
            ps.setInt(5, usuario.getId());
            ps.executeUpdate();
        }
    }

    public void eliminarUsuario(int id) throws SQLException {
        String consulta = "DELETE FROM usuario WHERE id = ?";
        try (PreparedStatement ps = conexion.prepareStatement(consulta)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
    }
}
