<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registro de Usuario</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Registro de Usuario</h1>
        <form action="${pageContext.request.contextPath}/UsuarioController" method="post">
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombre de Usuario:</label>
                <input type="text" class="form-control" id="nombre" name="nombre" required>
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Contraseña:</label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>
            <div class="mb-3">
                <label for="saldoInicial" class="form-label">Saldo Inicial:</label>
                <input type="number" class="form-control" id="saldoInicial" name="saldoInicial" required>
            </div>
            <input type="hidden" name="action" value="register">
            <button type="submit" class="btn btn-primary">Registrarse</button>
        </form>
        <p>${mensaje}</p>
        <a href="${pageContext.request.contextPath}/index.jsp" class="btn btn-link">Iniciar Sesión</a>
    </div>
</body>
</html>
