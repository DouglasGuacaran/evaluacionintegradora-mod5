<%@page import="dao.CuentaDAO"%>
<%@page import="conexion.ConexionBD"%>
<%@page import="java.sql.Connection"%>
<%@page import="models.Usuario"%>
<%@page import="java.sql.SQLException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="models.Cuenta"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Alke Wallet</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Bienvenido, ${sessionScope.usuario.nombre} ${sessionScope.usuario.apellido}</h1>
        <h2>Cuenta de ahorro del Usuario: ${sessionScope.usuario.nombre}</h2>

        <%
            Usuario usuario = (Usuario) session.getAttribute("usuario");
            Connection conn = null;
            try {
                conn = ConexionBD.getConexion();
                CuentaDAO cuentaDAO = new CuentaDAO(conn);
                double saldo = cuentaDAO.obtenerSaldo(usuario.getId());
        %>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><%= saldo %></td>
                </tr>
            </tbody>
        </table>
        <%
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        %>

        <hr>

        <h2>Realizar Dep�sito</h2>
        <form action="${pageContext.request.contextPath}/transaccion" method="post">
            <div class="mb-3">
                <label for="monto" class="form-label">Monto a depositar:</label>
                <input type="number" class="form-control" id="monto" name="monto" required>
                <input type="hidden" name="tipoOperacion" value="deposito">
            </div>
            <button type="submit" class="btn btn-primary">Depositar</button>
        </form>

        <hr>

        <h2>Realizar Retiro</h2>
        <form action="${pageContext.request.contextPath}/transaccion" method="post">
            <div class="mb-3">
                <label for="monto" class="form-label">Monto a retirar:</label>
                <input type="number" class="form-control" id="monto" name="monto" required>
                <input type="hidden" name="tipoOperacion" value="retiro">
            </div>
            <button type="submit" class="btn btn-primary">Retirar</button>
        </form>
        <p>${mensaje}</p>
    </div>
</body>
</html>
