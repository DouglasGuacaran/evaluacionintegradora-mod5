<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Menú Principal</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Inicio de Sesión</h1>
        <form action="${pageContext.request.contextPath}/UsuarioController" method="post">
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombre de Usuario:</label>
                <input type="text" class="form-control" id="nombre" name="nombre" required>
            </div>
            <div class="mb-3">
                <label for="contrasena" class="form-label">Contraseña:</label>
                <input type="password" class="form-control" id="contrasena" name="contrasena" required>
            </div>
            <input type="hidden" name="action" value="login">
            <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
        </form>
        <p>${mensaje}</p>
        <a href="${pageContext.request.contextPath}/registro.jsp" class="btn btn-link">Registrarse</a>
    </div>
</body>
</html>
